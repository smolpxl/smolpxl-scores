* [ ] Store the date/time a score is posted
* [ ] Allow providing "proof" that a score is valid
* [x] Set headers in PHP instead of Apache config
* [x] OpenAPI docs
* [x] Basic API up, backed by database
