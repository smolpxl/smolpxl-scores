all:
	@echo 'Try make upload'

upload:
	rsync -r api/ scores.artificialworlds.net:scores.artificialworlds.net/api/
	cp openapi-src/* openapi/
	rsync -r openapi/ scores.artificialworlds.net:scores.artificialworlds.net/openapi/

createdb:
	@echo
	@echo 'CREATE DB'
	@echo 'Before running this, add the password for "scoresadmin" to .my.cnf'
	@echo 'on scores.artificialworlds.net:'
	@echo
	@echo '    [client]'
	@echo '    password="password"'
	@echo
	@echo 'Warning: deletes ALL data in the database!'
	@./scripts/check_continue
	ssh scores.artificialworlds.net \
		mysql -u scoresadmin -h mysql.artificialworlds.net artific_scores \
		< db/schema.sql

openapi-update:
	wget 'https://github.com/swagger-api/swagger-ui/archive/v3.25.0.tar.gz' \
		-O swagger-ui.tar.gz
	tar -xzf swagger-ui.tar.gz
	rm swagger-ui.tar.gz
	rm -rf openapi
	mv swagger-ui-* openapi
	cp openapi-src/* openapi/
