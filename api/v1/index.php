<?php

include_once 'config.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');
header('Cache-Control: no-store, max-age=0');

// Note: the .htaccess file re-writes URLs like /myapp/mytable to look like
// ?app=myapp&?table=mytable

$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if (array_key_exists('table', $_GET)) {
        if (array_key_exists('file', $_GET)) {
            add_file();
        } else {
            add_score();
        }
    } else {
        add_table();
    }
} else if ('GET' === $method) {
    if (array_key_exists('fileid', $_GET)) {
        get_file();
    } else {
        get_scores();
    }
}

function self_url() {
    return (
        ( $_SERVER["HTTPS"] ? "https://" : "http://" )
        . $_SERVER["HTTP_HOST"]
        . $_SERVER["REQUEST_URI"]
    );
}

function bad_request($msg) {
    http_response_code(400);
    header('Content-Type: application/json');
    echo json_encode(['error' => $msg]);
}

function missing_app() {
    bad_request("You must provide an app name.");
}

function bad_app() {
    bad_request("App name is invalid");
}

function bad_table() {
    bad_request("Table name is invalid");
}

function bad_file() {
    bad_request("File name is invalid - must be exactly 'file'");
}

function bad_fileid() {
    bad_request("File id is invalid - must be an integer");
}

function bad_app_id() {
    bad_request("App id is invalid");
}

function bad_table_data() {
    bad_request(
        "You must provide a JSON object with these properties:
        appId: string (ASCII letters, numbers and dashes only)
        table: string (ASCII letters, numbers and dashes only, max 15)
        high_is_good: boolean
        "
    );
}

function bad_score_data() {
    bad_request(
        "You must provide a JSON object with these properties:
        appId: string (ASCII letters, numbers and dashes only)
        name: string (up to 15 characters of Unicode)
        score: number
        notes: string (up to 255 characters of Unicode)
        "
    );
}

function bad_file_data() {
    bad_request(
        "You must provide a JSON object with these properties:
        appId: string (ASCII letters, numbers and dashes only)
        contents: string (UTF-8, < 65536 code units)
        "
    );
}

function table_already_exists() {
    bad_request("Table already exists");
}

function table_not_found() {
    http_response_code(404);
    header('Content-Type: application/json');
    echo json_encode(['error' => 'Table not found']);
}

function player_not_found() {
    http_response_code(404);
    header('Content-Type: application/json');
    echo json_encode(['error' => 'startName not found']);
}

function file_not_found() {
    http_response_code(404);
    header('Content-Type: application/json');
    echo json_encode(['error' => 'file not found']);
}

function is_ascii_letters_numbers_and_dashes($input) {
    return is_string($input) and preg_match('/^[-a-zA-Z0-9]*$/', $input);
}

function is_valid_app_name($input) {
    return is_ascii_letters_numbers_and_dashes($input) and strlen($input) < 16;
}

function is_valid_app_id($input) {
    return is_ascii_letters_numbers_and_dashes($input) and strlen($input) < 37;
}

function is_valid_table($input) {
    return is_ascii_letters_numbers_and_dashes($input) and strlen($input) < 16;
}

function is_valid_file($input) {
    return $input === 'file';
}

function is_valid_fileid($input) {
    return is_numeric_int($input);
}

function is_valid_contents($input) {
    return strlen($input) < 65536;
}

function is_valid_name($input) {
    return is_string($input) and strlen($input) < 16;
}

function is_valid_score($input) {
    return is_float($input) or is_int($input);
}

function is_valid_notes($input) {
    return is_string($input) and strlen($input) < 256;
}

function is_numeric_int($input) {
    if (!is_numeric($input)) {
        return FALSE;
    }
    return is_int($input + 0);
}

function is_valid_rank($input) {
    return is_numeric_int($input) and ($input + 0) >= 1;
}

function is_valid_offset($input) {
    return is_numeric_int($input);
}

function is_valid_num($input) {
    if (!is_numeric_int($input)) {
        return FALSE;
    }
    $i = ($input + 0);
    return $i >= 1 and $i <= 50;
}

function is_existing_app($app) {
    GLOBAL $config;
    return array_key_exists($app, $config["apps"]);
}

function connect_to_db() {
    GLOBAL $config;

    $db = new PDO(
        "mysql:host=$config[mysql_host];dbname=$config[mysql_name]",
        $config['mysql_user'],
        $config['mysql_password']
    );
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $db;
}

function add_table() {
    if (!array_key_exists('app', $_GET)) {
        return missing_app();
    }

    $app = $_GET['app'];
    if (!( is_valid_app_name($app) and is_existing_app($app))) {
        return bad_app();
    }

    $body = json_decode(file_get_contents('php://input'));
    if (is_null($body)) {
        return bad_table_data();
    }

    $appId = $body->appId;
    $table = $body->table;
    $high_is_good = $body->high_is_good;

    if (
        !(
            is_valid_app_id($appId) and
            is_valid_table($table) and
            is_bool($high_is_good)
        )
    ) {
        return bad_table_data();
    }

    do_add_table($app, $appId, $table, $high_is_good);
}

function do_add_table($app, $appId, $table, $high_is_good) {
    GLOBAL $config;

    if ($appId !== $config["apps"][$app]["appId"]) {
        return bad_app_id();
    }

    $db = connect_to_db();

    $stmt = $db->prepare(
        'INSERT INTO sc_table (app_name, table_name, high_is_good) '
            . 'VALUES (?, ?, ?)'
    );

    try {
        $stmt->execute([$app, $table, $high_is_good]);
    } catch (PDOException $e) {
        // Note: deliberate ==.  === does not work since getCode() is string
        if (23000 == $e->getCode()) {
            return table_already_exists();
        } else {
            throw $e;
        }
    }

    http_response_code(204);
}

function add_score() {
    if (!array_key_exists('app', $_GET)) {
        return missing_app();
    }
    // table exists too or we would not have come in here

    $app = $_GET['app'];
    $table = $_GET['table'];
    if ( !(is_valid_app_name($app) and is_existing_app($app)) ) {
        return bad_app();
    }

    $db = connect_to_db();

    if ( !(is_valid_table($table)) ) {
        return bad_table();
    }

    $table_info = read_table_info($db, $app, $table);
    if (!$table_info) {
        return table_not_found();
    }

    $body = json_decode(file_get_contents('php://input'));
    if (is_null($body)) {
        return bad_score_data();
    }

    $appId = $body->appId;
    $name = $body->{"name"};
    $score = $body->score;
    $notes = $body->notes;

    if (
        !(
            is_valid_app_id($appId) and
            is_valid_name($name) and
            is_valid_score($score) and
            is_valid_notes($notes)
        )
    ) {
        return bad_score_data();
    }

    $score = floatval($score);  // Convert int to float

    do_add_score(
        $db, $app, $appId, $table, $table_info, $name, $score, $notes);
}

function add_file() {
    if (!array_key_exists('app', $_GET)) {
        return missing_app();
    }
    // table exists too or we would not have come in here
    // file exists too or we would not have come in here

    $app = $_GET['app'];
    $table = $_GET['table'];
    $file = $_GET['file'];
    if ( !(is_valid_app_name($app) and is_existing_app($app)) ) {
        return bad_app();
    }

    $db = connect_to_db();

    if ( !(is_valid_table($table)) ) {
        return bad_table();
    }

    if ( !(is_valid_file($file)) ) {
        return bad_file();
    }

    $table_info = read_table_info($db, $app, $table);
    if (!$table_info) {
        return table_not_found();
    }

    $body = json_decode(file_get_contents('php://input'));
    if (is_null($body)) {
        return bad_file_data();
    }

    $appId = $body->appId;
    $contents = $body->contents;

    if (
        !(
            is_valid_app_id($appId) and
            is_valid_contents($contents)
        )
    ) {
        return bad_file_data();
    }

    do_add_file($db, $app, $appId, $table, $contents);
}

function do_add_file(
    $db, $app, $appId, $table, $contents
) {
    GLOBAL $config;

    if ($appId !== $config["apps"][$app]["appId"]) {
        return bad_app_id();
    }

    $stmt = $db->prepare(
        'INSERT INTO sc_file '
            . '(app_name, table_name, contents) '
            . 'VALUES (?, ?, ?)'
    );

    $stmt->execute(
        [
            $app, $table, $contents,
        ]
    );

    $rowCount = $stmt->rowCount();
    if (0 === $rowCount) {
        // Failed to insert!
        http_response_code(500);
        echo json_encode(["error" => "Failed to create row for file"]);
    } else {
        if (1 === $rowCount) {
            // 1 row affected means we did an insert
            http_response_code(201);
            header('Content-type: application/json');
            $id = $db->lastInsertId();
            echo json_encode(["id"=> $id]);
        } else {
            // Weirdly, inserted multiple rows!
            http_response_code(501);
            echo json_encode([
                "error" =>
                    "Unexpectedly, multiple rows were inserted creating file"
            ]);
        }
    }
}

function do_add_score(
    $db, $app, $appId, $table, $table_info, $name, $score, $notes
) {
    GLOBAL $config;

    if ($appId !== $config["apps"][$app]["appId"]) {
        return bad_app_id();
    }

    $high_is_good = $table_info['high_is_good'];
    $cmp = $high_is_good ? ">" : "<";

    $stmt = $db->prepare(
        'INSERT INTO sc_score '
            . '(app_name, table_name, player_name, score, notes) '
            . 'VALUES (?, ?, ?, ?, ?) '
            . 'ON DUPLICATE KEY UPDATE '
                . 'notes = IF(?' . $cmp . 'score,?,notes), '
                . 'score = IF(?' . $cmp . 'score,?,score)'
    );

    try {
        $stmt->execute(
            [
                $app, $table, $name, $score, $notes,
                $score, $notes,
                $score, $score
            ]
        );
    } catch (PDOException $e) {
        // Note: deliberate ==.  === does not work since getCode() is string
        if (23000 == $e->getCode()) {
            return table_already_exists();
        } else {
            throw $e;
        }
    }

    $rowCount = $stmt->rowCount();
    if (0 === $rowCount) {
        // We didn't change anything.  We use 204 to indicate that, which
        // may be a little weird, but it's nice to make it clear to clients.
        http_response_code(204);
        // No response body, since that is what 204 means.
    } else {
        if (1 === $rowCount) {
            // 1 row affected means we did an insert
            http_response_code(201);
        } else {
            // 2 rows from MySQL indicates the update happened
            http_response_code(200);
        }
        // Return the data, only if something changed
        header('Content-type: application/json');
        echo json_encode(
            ["name"=> $name, "score"=> $score, "notes" => $notes]);
    }
}

function read_table_info($db, $app, $table) {
    $stmt = $db->prepare(
        'SELECT high_is_good '
            . 'FROM sc_table '
            . 'WHERE app_name = ? '
            . 'AND table_name = ?'
    );
    $stmt->execute([$app, $table]);
    $row = $stmt->fetch(PDO::FETCH_NUM);

    if ($row) {
        return ['high_is_good' => $row[0]];
    } else {
        return null;
    }
}

function get_scores() {
    $app = $_GET['app'];
    $table = $_GET['table'];
    if ( !(is_valid_app_name($app) and is_existing_app($app)) ) {
        return bad_app();
    }

    if (!is_valid_table($table)) {
        return bad_table();
    }

    $db = connect_to_db();

    $table_info = read_table_info($db, $app, $table);
    if (!$table_info) {
        return table_not_found();
    }

    if (array_key_exists('startName', $_GET)) {
        $startName = $_GET['startName'];
        if (array_key_exists('startRank', $_GET)) {
            return bad_request(
                "You may include startName OR startRank, not both");
        }
        $startRank = null;
        if (!is_valid_name($startName)) {
            return bad_request(
                "startName must be Unicode up to 255 characters");
        }
    } else {
        $startName = null;
        if (array_key_exists('startRank', $_GET)) {
            $startRank = $_GET['startRank'];
            if (!is_valid_rank($startRank)) {
                return bad_request("startRank must be an integer >= 1");
            }
            $startRank = intval($startRank);
        } else {
            $startRank = 1;
        }
    }

    if (array_key_exists('offset', $_GET)) {
        $offset = $_GET['offset'];
        if (!is_valid_offset($offset)) {
            return bad_request("offset must be an integer");
        }
        $offset = intval($offset);
    } else {
        $offset = 0;
    }

    if (array_key_exists('num', $_GET)) {
        $num = $_GET['num'];
        if (!is_valid_num($num)) {
            return bad_request("num must be an integer >= 1 and <= 50");
        }
        $num = intval($num);
    } else {
        $num = 10;
    }

    if ($startName) {
        $results = do_get_scores_by_name(
            $db, $app, $table, $table_info, $startName, $offset, $num);
    } else {
        $results = do_get_scores_by_rank(
            $db, $app, $table, $table_info, $startRank, $offset, $num);
    }

    if (null === $results) {
        return;
    }

    // TODO: next and previous links
    $response = [
        'results' => $results,
        'app' => $app,
        'table' => $table,
        'links' => [
            'self' => self_url()
        ]
    ];
    header('Content-type: application/json');
    echo json_encode( $response );
}

function ranked_scores_query($high_is_good) {
    return (
        '(SELECT '
               . 'ROW_NUMBER() OVER ('
                   . 'ORDER BY score '
                   . ($high_is_good ? 'DESC' : 'ASC')
               . ") AS 'rank', "
               . 'player_name, '
               . 'score, '
               . 'notes '
            . 'FROM '
                . 'sc_score '
            . 'WHERE app_name = ? '
            . 'AND table_name = ? '
            . 'ORDER BY score '
            . ($high_is_good ? 'DESC' : 'ASC')
        . ') x '
    );
}

function do_get_scores_by_name(
    $db, $app, $table, $table_info, $startName, $offset, $num) {

    $stmt = $db->prepare(
        'SELECT x.rank FROM '
            . ranked_scores_query($table_info['high_is_good'])
            . 'WHERE player_name = ?'
    );

    $stmt->execute([$app, $table, $startName]);

    $row = $stmt->fetch(PDO::FETCH_NUM);

    if (!$row) {
        return player_not_found();
    }

    return do_get_scores_by_rank(
        $db, $app, $table, $table_info, $row[0], $offset, $num);
}

function do_get_scores_by_rank(
    $db, $app, $table, $table_info, $startRank, $offset, $num) {

    $start = $startRank + $offset;
    if ($start < 1) {
        $start = 1;
    }

    $stmt = $db->prepare(
        'SELECT * FROM '
            . ranked_scores_query($table_info['high_is_good'])
            . 'WHERE x.rank BETWEEN ? AND ?'
    );

    $stmt->execute([$app, $table, $start, $start + $num - 1]);

    $ret = $stmt->fetchAll(PDO::FETCH_ASSOC);

    function cmp_by_rank($a, $b) {
        return $a["rank"] <=> $b["rank"];
    }
    usort($ret, "cmp_by_rank");

    return $ret;
}

function get_file() {
    $app = $_GET['app'];
    $table = $_GET['table'];
    $file = $_GET['file'];
    $fileid = $_GET['fileid'];
    if ( !(is_valid_app_name($app) and is_existing_app($app)) ) {
        return bad_app();
    }

    if (!is_valid_table($table)) {
        return bad_table();
    }

    if(!is_valid_file($file)) {
        return bad_file();
    }

    if(!is_valid_fileid($fileid)) {
        return bad_fileid();
    }

    $db = connect_to_db();

    $result = do_get_file($db, $app, $table, $fileid);

    if (null === $result) {
        return;
    }

    $response = [
        'app' => $app,
        'table' => $table,
        'id' => $fileid,
        'contents' => $result['contents']
    ];
    header('Content-type: application/json');
    echo json_encode( $response );
}

function do_get_file($db, $app, $table, $fileid) {
    $stmt = $db->prepare(
        'SELECT contents FROM sc_file '
            . 'WHERE app_name = ? '
            . '  AND table_name = ? '
            . '  AND id = ? '
    );

    $stmt->execute([$app, $table, $fileid]);

    $row = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$row) {
        return file_not_found();
    }

    return $row;
}
