<?php

$config = [
    "mysql_host" => "myhost",
    "mysql_name" => "mydbname",
    "mysql_user" => "myuser",
    "mysql_password" => "mypassword",

    "apps" => [
        "myappname" => [
            "appId" => "myappid",
            "owner" => "Andy Balaam <andybalaam@example.com>",
            "www"   => "http://example.com"
        ],
        "app2" => [
            "appId" => "myappid2",
            "owner" => "Andy Balaam <andybalaam@example.com>",
            "www"   => "http://example.com"
        ]
    ]
];
