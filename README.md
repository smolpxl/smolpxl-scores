# Smolpxl Scores

Smolpxl Scores is a high-score table server for Free and Open Source games.

## Adding a high score table to your game

If you'd like to use Smolpxl Scores to add a high score table to your game,
follow these instructions.

* Create an issue requesting an app name and ID for your game.  (Only Free and
  Open Source games will be considered.)

* When you have an app name and ID, you can try out the API on the
  [Smolpxl Scores interactive OpenAPI/Swagger](https://scores.artificialworlds.net/openapi/)
  page.

* Or, on the command line, create a new high score table like this:

```bash
curl -i https://scores.artificialworlds.net/api/v1/myappname/ -d \
    '{"appId":"myappid","table":"mytablename","high_is_good": true}'
```

Note on `high_is_good` - if you score points for doing good things in your
game, then `high_is_good` should be `true`, and if scores are things like
times, where a lower number is better, then `high_is_good` should be `false`.

* Now you can post new scores like this:

```bash
curl -i https://scores.artificialworlds.net/api/v1/myappname/mytablename/ -d \
    '{"appId":"myappid","name":"Megan Tria", "score": 13.5, "notes": ""}'
```

Note: "score" must be a single number (integer or floating-point), and this
number will be used to decide each player's rank.  If a score is made up of
multiple values (such as time+retries), you must work out a way to represent
this as a single number (e.g. retries x 100 + time), but you can record
more information as a string in "notes".

Response codes:

```
200 - a score was updated (the new score was better than the existing one)
201 - a new score was added (previously there was no score for this player)
204 - no change was made (this player already has a better score)
```

* And you can ask about scores like this:

Find the top ten players:

```bash
$ curl 'https://scores.artificialworlds.net/api/v1/myappname/mytablename/'
{
  "results": [
    {
      "rank": "1",
      "player_name": "Sir Robert Peel",
      "score": "100",
      "notes": ""
    },
...
    {
      "rank": "10",
      "player_name": "Robert Gascoyne",
      "score": "91",
      "notes": ""
    }
  ],
  "app": "myappname",
  "table": "mytablename",
  "links": {
    "self": "https://scores.artificialworlds.net/api/v1/myappname/mytablename/"
  }
}
```

Find the next 20:

```bash
$ curl 'https://scores.artificialworlds.net/api/v1/myappname/mytablename/?startRank=11&num=20'
{
  "results": [
    {
      "rank": "11",
      "player_name": "Arthur Balfour",
      "score": "90",
      "notes": ""
    },
...
    {
      "rank": "30",
      "player_name": "Gordon Brown",
      "score": "71",
      "notes": ""
    }
  ],
  "app": "myappname",
  "table": "mytablename",
  "links": {
    "self": "https://scores.artificialworlds.net/api/v1/myappname/mytablename/?startRank=11&num=20"
  }
}
```

Find the scores surrounding one player:

```bash
$ curl 'https://scores.artificialworlds.net/api/v1/myappname/mytablename/?startName=David%20Lloyd%20Geo&offset=-5&num=10'
{
  "results": [
    {
      "rank": "9",
      "player_name": "Archibald Primr",
      "score": "92",
      "notes": ""
    },
...
    {
      "rank": "14",
      "player_name": "David Lloyd Geo",
      "score": "87",
      "notes": ""
    },
...
    {
      "rank": "18",
      "player_name": "Neville Chamber",
      "score": "83",
      "notes": ""
    }
  ],
  "app": "myappname",
  "table": "mytablename",
  "links": {
    "self": "https://scores.artificialworlds.net/api/v1/myappname/mytablename/?startName=David%20Lloyd%20Geo&offset=-5&num=10"
  }
}
```

* To upload proof of a score:

```bash
$ curl https://scores.artificialworlds.net/api/v1/myappname/mytablename/file -d \
    '{"appId":"myappid","contents":"PROOF CONTENTS"}'

{"id":"123"}
```

The value of `contents` may be any JSON (UTF-8) string up to 65536 bytes.

* To download the proof later:

```bash
$ curl https://scores.artificialworlds.net/api/v1/myappname/mytablename/file/123
{
  "app": "myappname",
  "table": "mytablename",
  "id": "123",
  "contents": "PROOF CONTENTS"
}
```

## Allowed names

All API requests and responses must be UTF-8 encoded.

App names and table names may be ASCII letters and numbers, and dashes only,
with a maximum of 15 characters.

Player names may be any Unicode string, with a maximum of 15 characters. If
you think this is a little short, you are probably right, but the intention
is to assist game developers to fit names on small screens.  Hey, in the old
games you only got 3 letters!

Notes may be any Unicode string, with a maximum of 255 characters.

## Deploy your own high scores server

If you'd like to make your own high scores server, instead of using the one
at [scores.artificialworlds.net](https://scores.artificialworlds.net), follow
these instructions.

Note: The deployment scripts were written for my own benefit, so may need some
tweaking for your uses.  Please contact me if you'd like to help improve them.

* Prerequisites:
    * Apache web server with mod_php
    * MySQL

* Create a database in MySQL

* Modify Makefile's `createdb` target to include your DB details then run:

```bash
make createdb
```

This will create the tables in your database.

* Set up DB config for the API:

```bash
cp api/v1/example-config.php api/v1/config.php
# Edit api/v1/config.php to enter database details and allowed apps
```

* Modify Makefile's `upload` target to include your web server then run:

```bash
make upload
```

This will upload the PHP files to your web server.

The API should be ready to run.

## License

Smolpxl Scores is copyright 2020 Andy Balaam and the Smolpxl Scores
contributors.

It is released under the [AGPL license](LICENSE), version 3 or later.
