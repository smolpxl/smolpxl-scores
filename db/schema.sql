DROP TABLE IF EXISTS sc_table;

CREATE TABLE sc_table (
    app_name VARCHAR(15) NOT NULL,
    table_name VARCHAR(15),
    high_is_good BOOLEAN NOT NULL DEFAULT 1,
    PRIMARY KEY(app_name, table_name)
);


DROP TABLE IF EXISTS sc_score;

CREATE TABLE sc_score (
    app_name VARCHAR(15),
    table_name VARCHAR(15),
    player_name VARCHAR(15),
    score DOUBLE,
    notes VARCHAR(255),
    PRIMARY KEY(app_name, table_name, player_name),
    INDEX(app_name, table_name, score)
);

DROP TABLE IF EXISTS sc_file;

CREATE TABLE sc_file (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    app_name VARCHAR(15),
    table_name VARCHAR(15),
    contents TEXT,
    PRIMARY KEY(id)
);
